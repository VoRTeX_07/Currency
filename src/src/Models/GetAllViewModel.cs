﻿namespace src.Views.Valute;

public class GetAllViewModel
{
    public IEnumerable<KeyValuePair<string, Currency.DL.Valute>> Valutes { get; set; }
    public PageViewModel PageViewModel { get; set; }
}