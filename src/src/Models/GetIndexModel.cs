﻿namespace src.Models;

public class GetIndexModel
{
    public IEnumerable<KeyValuePair<string, Currency.DL.Valute>> Valutes { get; set; }
}