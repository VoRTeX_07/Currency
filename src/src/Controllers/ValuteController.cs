﻿using Microsoft.AspNetCore.Mvc;
using Currency.BL;
using src.Models;
using src.Views.Valute;

namespace src.Controllers
{
    public class ValuteController : Controller
                {
                    private readonly IValuteService _service;

                    public ValuteController(IValuteService service)
                    {
                        _service = service;
                    }

                    public async Task<ActionResult> Index()
                    {
                        try
                        {
                            var allValute = await _service.GetAllAsync();
                            GetIndexModel getIndexModel = new GetIndexModel
                            {
                                Valutes = allValute
                            };
                            return View(getIndexModel);
                        }
                        catch(Exception e)
                        {
                            return ViewBag["Error"] = e.Message;
                        }

                    }

                    [HttpGet]           
                    public async Task<ActionResult> Get(string key)
                    {
                        if (string.IsNullOrWhiteSpace(key))
                        {
                            return ViewBag["Error"] = "Выбирите валюту из списка";
                        }
                        else
                        {
                            
                            var result = await _service.GetAsync(key);
                            return View(result);
                            
                        }
                    }
                    [HttpGet]
                    public async  Task<ActionResult> GetAll(int page = 1)
                    {
                        try
                        {
                            var size = 5;
                            var allValute = await _service.GetAllAsync();
                            var count = allValute.Count();
                            var items = allValute.Skip((page - 1) * size).Take(size);
                            PageViewModel pageViewModel = new PageViewModel(count, page, size);
                            GetAllViewModel viewModel = new GetAllViewModel
                            {
                                PageViewModel = pageViewModel,
                                Valutes = items
                            };
                            return View(viewModel);
                        }
                        catch(Exception e)
                        {
                            return ViewBag["Error"] = e.Message;
                        }
                    }
                }
}