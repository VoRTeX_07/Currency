﻿using Newtonsoft.Json;

namespace Currency.DL;

public class ValuteSet
{
    [JsonProperty("Valute")]
    public Dictionary<string, Valute>? Valutes { get; set; }
}