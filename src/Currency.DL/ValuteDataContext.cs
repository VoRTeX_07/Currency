﻿namespace Currency.DL;

public class ValuteDataContext
{
    public async Task<ValuteSet?> GetDataContextAsync()
    {
        var url = "https://www.cbr-xml-daily.ru/daily_json.js";
        try
        {
            using (HttpClient client = new HttpClient())
            {
                using (var response = await client.GetAsync(url))
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<ValuteSet>(json);
                    return result;
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}