﻿using Currency.DL;


namespace Currency.BL;

public class ValuteService : IValuteService
{
    
    private readonly ValuteDataContext _valuteDataContext;

    public ValuteService(ValuteDataContext valuteDataContext)
    {
        _valuteDataContext = valuteDataContext;
    }
    /// <summary>
    /// Получение всего списка валют.
    /// </summary>
    /// <param name="page"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    public async Task<Dictionary<string,Valute>> GetAllAsync()
    {
        var listValute = await _valuteDataContext.GetDataContextAsync();
        return listValute.Valutes;
    }
    /// <summary>
    /// Получение информации,выбранной валюты.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<Valute> GetAsync(string key)
    {
        var result = await _valuteDataContext.GetDataContextAsync().ConfigureAwait(false);
        return result.Valutes[key];
    }
}