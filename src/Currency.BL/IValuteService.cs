﻿using Currency.DL;

namespace Currency.BL;

public interface IValuteService
{
   Task<Dictionary<string,Valute>> GetAllAsync();

    Task<Valute> GetAsync(string key);
}